package com.ciit.training.staff.service;

import java.util.List;

import com.ciit.training.staff.dao.EmployeeMongoRepo;
import com.ciit.training.staff.model.Employee;

import org.springframework.beans.factory.annotation.Autowired;

public class EmployeeService {

    @Autowired
    private EmployeeMongoRepo mongoRepo;

    public List<Employee> findAll(){

        return mongoRepo.findAll();
        
    }

    public Employee save(Employee employee){
        return mongoRepo.save(employee);
    }



    


    
}
